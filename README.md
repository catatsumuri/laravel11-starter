# Getting Started with This Package

## Setting Up Environment Variables

1. Copy `.env.example` to `.env` by running the following command:

    ```bash
    cp .env.example .env
    ```

## Installing Dependencies

- Proceed with installing the necessary PHP packages using Composer:

    ```bash
    composer install --ignore-platform-reqs
    ```

    Alternatively, if you prefer using Docker, you can run:

    ```bash
    docker run --rm -it -v $(pwd):/app composer install --ignore-platform-reqs
    ```

## Launching Laravel Sail

- To start Laravel Sail, execute the following command:

    ```bash
    ./vendor/bin/sail up
    ```

### Generating Application Key

- Next, generate the `APP_KEY` using:

    ```bash
    ./vendor/bin/sail artisan key:generate
    ```

### Database Migration

- Lastly, migrate your database to ensure it's up to date:

    ```bash
    ./vendor/bin/sail artisan migrate
    ```

Follow these steps to set up and start using the package.

